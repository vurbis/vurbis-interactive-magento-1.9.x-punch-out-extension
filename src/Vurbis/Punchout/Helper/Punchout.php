<?php
class Vurbis_Punchout_Helper_Punchout extends Mage_Core_Helper_Abstract
{

    public function getApiUrl()
    {
        $apiUrl = Mage::getStoreConfig('punchout_options/api_group/api_url', Mage::app()->getStore());
        if (empty($apiUrl)) {
            $apiUrl = "https://ioedeveloper.com/api";
        }
        return $apiUrl;
    }

    public function getSupplierId()
    {
        return Mage::getStoreConfig('punchout_options/api_group/supplier_id', Mage::app()->getStore());
    }

    /**
     * @todo send request to api url
     * @param string $url
     * @param array $data
     * @param string $format
     * @param string $response
     * @return string|mixed
     */
    public function post($url, $data = null, $format = 'json', $response = 'json')
    {
        $headers = [
            'Accept: application/' . $response,
            'Content-Type: application/' . $format,
        ];
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_POST, true);
        if ($format == 'json' && isset($data)) {
            $data = json_encode($data);
        }
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        if ($response == 'json') {
            $response = json_decode(curl_exec($handle));
        } else {
            $response = curl_exec($handle);
        }
        curl_close($handle);
        if (isset($response->error) && isset($response->message)) {
            throw new Exception($response->message);
        }
        return $response;
    }
}
