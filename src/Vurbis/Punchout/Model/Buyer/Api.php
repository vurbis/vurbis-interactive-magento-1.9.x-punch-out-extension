<?php
class Vurbis_Punchout_Model_Buyer_Api extends Mage_Api_Model_Resource_Abstract
{
  public function update($buyerId,array $props,$storeId)
  {
    $results = array();
    foreach ($props as $prop) {
        $table = $prop->table;
        $field = $prop->field;
        $value = $prop->value;
        $selector = 'entity_id';
        if(isset($prop->selector)){
            $selector = $prop->selector;
        }
        $kind = $prop->kind;
        try {
            if ($field == 'password'){
                $buyer = Mage::getModel('customer/customer')->load($buyerId);
                $buyer->setPassword($value);
                $buyer->save();
                array_push($results,array("result" => true, "field"=> $field, "error" => ""));
            } else if($kind == 'entity_field') {
                if (!isset($table)) {
                    $table = "customer/entity";
                }
                $resource = Mage::getSingleton('core/resource');
                $write = $resource->getConnection('core_write');
                $table = $resource->getTableName($table);
                $data = array();
                $data[$field] = $value;
                $key = $selector.' = ?';
                $write->update($table, $data, [ $key => $buyerId ]);
                array_push($results,array("result" => true, "field"=> $field, "error" => "updated table: ".$table));
            } else if($kind == 'attribute'){
                if (!isset($table)) {
                    $table = "customer/customer";
                }
                $buyer = Mage::getModel($table)->load($buyerId);
                $attribute = $buyer->getResource()->getAttribute($field);
                if ($attribute) {
                    $buyer->setData($field,$value);
                    $buyer->getResource()->saveAttribute($buyer, $field);
                    array_push($results,array("result" => true, "field"=> $field, "error" => ""));
                }else{
                    array_push($results,array("result" => false, "error" => "Attribute ".$field." not found", "field"=> $field ));
                }
            } else {
                array_push($results,array("result" => false, "error" => "Kind ".$kind." is not supported", "field"=> $field ));
            }
        } catch (Exception $e) {
            array_push($results,array("result" => false, "error" => "exception: ".$e->getMessage(), "field"=> $field ));
        }
    }
    return $results;
  }

  public function quoteitemupdate($quoteId,$sku,array $props,$storeId)
  {
    $results = array();
    $quote = Mage::getModel('sales/quote')->setStoreId($storeId)->load($quoteId);
    if($quote) {
        $cartItems = $quote->getAllItems();
        $exists;
        foreach ($cartItems as $item) {
            if($sku == $item->getSku()){
                $exists = $item;
            }
        }
        if($exists) {
            foreach ($props as $prop) {
                $field = $prop->field;
                $value = $prop->value;
                $kind = $prop->kind;
                try {
                    $exists->setData($field,$value);
                    $exists->save();
                    array_push($results,array("result" => true, "field"=> $field, "error" => ""));
                } catch (Exception $e) {
                    array_push($results,array("result" => false, "error" => "msg: ".$e->getMessage(), "field"=> $field ));
                }
            }
        }else {
            array_push($results,array("result" => false, "error" => "Product Not Found ".$sku, "field"=> "QUOTE" ));
        }
    }else {
        array_push($results,array("result" => false, "error" => "Quote Not Found ".$quoteId, "field"=> "QUOTE" ));
    }
    return $results;
  }
}