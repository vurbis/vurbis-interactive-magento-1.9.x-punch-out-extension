<?php
/**
 * Observer runs upong event: controller_front_send_response_before
 */
class Vurbis_Punchout_Model_Observer extends Varien_Event_Observer
{
    public function __construct()
    {
    }

    /**
     * Adds the script tag to include the js file required for punchout to work
     *
     * @param Observer $observer the observer object
     *
     * @return null
     */
    public function addScript($observer)
    {
        $punchout = Mage::helper('punchout/punchout');
        $session = Mage::getSingleton('customer/session');
        $sessionId = $session->getPunchoutSessionId();
        if (!empty($sessionId)) {
            $cart = Mage::getModel('checkout/cart')->getQuote();
            $supplierId = $punchout->getSupplierId();
            $apiUrl = $punchout->getApiUrl();
            $response = $observer->getData('front')->getResponse();
            $html = $response->getBody();
            $params = "?cart=".$cart->getId();
            $html = str_replace("</body>", '<script>initPunchout()</script></body>', $html);
            $html = str_replace("</head>", '<script src="'.$apiUrl.'/punchout/files/'.$sessionId.'/magento1.js'.$params.'"></script></head>', $html);
            $response->setBody($html);
        }
    }

}
