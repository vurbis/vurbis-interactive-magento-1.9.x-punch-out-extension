<?php
namespace Vurbis\Punchout\Composer;

use Composer\Installer\PackageEvent;

class Magento
{
    /**
     * Copy the Vurbis Punchout module into the appropriate Magento app folder
     *
     * @param PackageEvent $event
     */
    public static function postPackageAction(PackageEvent $event)
    {
        $extras = $event->getComposer()->getPackage()->getExtra();

        if (isset($extras['magento-root-dir'])) {
            $magentoPath = $extras['magento-root-dir'];
            if (is_dir($magentoPath . 'app/code/community/Vurbis/Punchout')) {
                self::_recursiveRmDir($magentoPath . 'app/code/community/Vurbis/Punchout');                
            }
            copy(dirname(__DIR__).'/Vurbis_Punchout.xml', $magentoPath . 'app/etc/modules/Vurbis_Punchout.xml');
            unlink(dirname(__DIR__).'/Vurbis_Punchout.xml');
            self::_recurseCopy(dirname(__DIR__), $magentoPath . 'app/code/community/Vurbis/Punchout');
            self::_recurseCopy(dirname(__DIR__).'/locale', $magentoPath . 'app/locale');
            self::_recursiveRmDir($magentoPath . 'app/code/community/Vurbis/Punchout/Composer');
            self::_recursiveRmDir($magentoPath . 'app/code/community/Vurbis/Punchout/locale');
        }
    }

    /**
     * Remove the installed module Vurbis Punchout from the app/community Magento folder
     *
     * @param PackageEvent $event
     */
    public static function cleanPackageAction(PackageEvent $event)
    {
        $extras = $event->getComposer()->getPackage()->getExtra();
        if (isset($extras['magento-root-dir'])) {
            $magentoPath = $extras['magento-root-dir'];
            if (is_dir($magentoPath . 'app/code/community/Vurbis/Punchout')) {
                self::_recursiveRmDir($magentoPath . 'app/code/community/Vurbis/Punchout');
            }
            unlink($magentoPath . 'app/etc/modules/Vurbis_Punchout.xml');
            if(self::_isDirEmpty($magentoPath . 'app/code/community/Vurbis')) {
                rmdir($magentoPath . 'app/code/community/Vurbis');
            }
            // remove translate file
            if(file_exists($magentoPath . 'app/locale/en_US/Vurbis_Punchout.csv')) {
                unlink($magentoPath . 'app/locale/en_US/Vurbis_Punchout.csv');
            }
        }
    }

    /**
     * Copy recursively the source to a target
     *
     * @param string $src
     * @param string $dst
     */
    protected static function _recurseCopy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst, 0755, true);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    self::_recurseCopy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    /**
     * Remove directory recursively
     *
     * @param $dir
     * @return bool
     */
    public static function _recursiveRmDir($dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? self::_recursiveRmDir("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }
    
    public static function _isDirEmpty($dir) {
        if (!is_readable($dir)) return NULL; 
        return (count(scandir($dir)) == 2);
    }
}