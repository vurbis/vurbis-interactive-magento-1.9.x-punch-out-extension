<?php
class Vurbis_Punchout_RequestController extends Mage_Core_Controller_Front_Action
{
    public function getActionMethodName($action)
    {
        return 'indexAction';
    }

    public function indexAction()
    {
        $punchout = Mage::helper('punchout/punchout');
        $apiUrl = $punchout->getApiUrl();
        $url = Mage::helper('core/url')->getCurrentUrl();
        $path = explode("/punchout", $url)[1];
        $body = file_get_contents('php://input');
        $url = $apiUrl .'/punchout'. $path;
        $res = $punchout->post($url, $body, "xml", "xml");
        $this->getResponse()->clearHeaders()
        ->setHeader('Content-Type', 'text/xml')
        ->setBody($res);
    }

}
