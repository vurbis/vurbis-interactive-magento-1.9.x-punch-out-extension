<?php
class Vurbis_Punchout_AutologinController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        try {
            $session = Mage::getSingleton('customer/session');
            $req = $this->getRequest();

            $punchoutSession = $req->getParam('session');
            $username = $req->getParam('username');
            $password = $req->getParam('password');
            $hook_url = $req->getParam('HOOK_URL');
            if (!$punchoutSession) {
                if (!$hook_url) {
                    throw new \Exception(Mage::helper('punchout')->__('HOOK_URL is required.'));
                }
                $punchout = Mage::helper('punchout/punchout');
                $apiUrl = $punchout->getApiUrl();
                $supplier_id = $punchout->getSupplierId();
                $url = $apiUrl . "/punchout/oci/" . $supplier_id . "/login";
                $res = $punchout->post($url, array(
                    'username' => $username,
                    'password' => $password,
                    'hook_url' => $hook_url
                ));
                if (!isset($res->id)) {
                    throw new \Exception(Mage::helper('punchout')->__('Username and password could not be found in Vurbis marketplace.'));                    
                }
                $punchoutSession = $res->id;
                $username = $res->username;
                $password = $res->password;
            }
            if (!$punchoutSession) {
                throw new \Exception(Mage::helper('punchout')->__('Punchout session is required.'));
            }
            $session->login($username, $password);
            $session->setPunchoutSessionId($punchoutSession);
            return $this->_redirect('/');
        } catch (\Exception $e) {
            Mage::getSingleton('core/session')->addError($e->getMessage());
            return $this->_redirect('/');
        }
    }
}
