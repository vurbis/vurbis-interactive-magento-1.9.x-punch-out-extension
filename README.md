# Installation Vurbis Module in Magento 1.x
## Using modman
In the root path of magento installation.

### Install
```
- modman clone https://gitlab.com/vurbis/vurbis-interactive-magento-1.9.x-punch-out-extension.git
- Clear caches
```
### Update
```
- modman update vurbis-interactive-magento-1.9.x-punch-out-extension
- Clear caches
```
### Remove
```
- modman undeploy vurbis-interactive-magento-1.9.x-punch-out-extension
- modman remove vurbis-interactive-magento-1.9.x-punch-out-extension
- Clear caches
```
## Using composer.json
Make sure composer.json has the extra option <br>
```
"extra":{
        "magento-root-dir":"./"
    },
"scripts": {
   "post-package-install": [
       "Vurbis\\Punchout\\Composer\\Magento::postPackageAction"
   ],
   "post-package-update": [
       "Vurbis\\Punchout\\Composer\\Magento::postPackageAction"
   ],
   "pre-package-uninstall": [
       "Vurbis\\Punchout\\Composer\\Magento::cleanPackageAction"
   ]
}
```

### Install
```
- composer require vurbis/magento-punchout
- Clear caches
```
### Update 
```
- composer update vurbis/magento-punchout
- Clear caches
```
### Remove
```
- composer remove vurbis/magento-punchout
- Clear cache
```
## Using package .zip
### Install and update
```
- Extract the package and upload to {YOUR-MAGENTO-ROOT-DIR}/app/code/community/Vurbis/Punchout
- Clear caches
```
### Remove
```
- remove files at {YOUR-MAGENTO-ROOT-DIR}/app/code/community/Vurbis/Punchout and {YOUR-MAGENTO-ROOT-DIR}/app/etc/modules/Vurbis_Punchout.xml
- Clear cache
```
---

https://www.vurbis.com/

Copyright © 2019 Vurbis. All rights reserved.  

![Vurbis Logo](https://www.vurbis.com/wp-content/uploads/2019/05/VURBIS-INTERACTIVE_trans_logo.png)